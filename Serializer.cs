﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Collections;

namespace SimpleJSON
{
    public class JSONSerializer
    {
        public const string UnSupportTypes = "Unsupported types";
        static Type NullableType = typeof(Nullable<>);

        public static string ToJSON(object obj)
        {
            var type = obj.GetType();
            if (obj==null)
            {
                throw new ArgumentNullException("obj");
            }
            if (obj is string || obj is decimal || (obj is ValueType && type.IsPrimitive))
            {
                return UnSupportTypes;
            }
            StringBuilder textBuilder = new StringBuilder();
            ToJSONImpl(obj, textBuilder);
            return textBuilder.ToString();
        }

        internal static void ToJSONImpl(object obj, StringBuilder textBuilder)
        {
            var type = obj.GetType();
            var num = 0;
            if (obj is IEnumerable)
            {
                textBuilder.Append("[");
                foreach (object item in obj as IEnumerable)
                {
                    if (item is string || item is DateTime)
                    {
                        textBuilder.Append("\"" + item.ToString() + "\",");
                    }
                    else if (item is decimal || (item is ValueType && item.GetType().IsPrimitive))
                    {
                        textBuilder.Append(item.ToString() + ",");
                    }
                    else
                    {
                        ToJSONImpl(item, textBuilder);
                        textBuilder.Append(",");
                    }
                    ++num;
                }
                if (num > 0)
                {
                    textBuilder.Remove(textBuilder.Length - 1, 1);
                }
                textBuilder.Append("],");
            }
            else
            {
                textBuilder.Append("{");
                var properties = type.GetProperties();
                foreach (var property in properties)
                {
                    var propObj = property.GetValue(obj);
                    if (propObj == null)
                    {
                        textBuilder.Append("\"" + property.Name + "\":null,");
                    }
                    else
                    {
                        textBuilder.Append("\"" + property.Name + "\":");
                        if (propObj is string || propObj is Guid || property.PropertyType.IsEnum || propObj is DateTime)
                        {
                            textBuilder.Append("\"" + propObj.ToString() + "\",");
                        }
                        else if (propObj is decimal
                          || (propObj is ValueType && property.PropertyType.IsPrimitive)
                          || (property.PropertyType.IsGenericType && property.PropertyType.GetGenericTypeDefinition() == NullableType))
                        {

                            textBuilder.Append(propObj.ToString().Replace("True", "true").Replace("False", "false") + ",");
                        }
                        else
                        {
                            ToJSONImpl(propObj, textBuilder);
                        }
                    }
                    
                    ++num;
                }
                if (num > 0 && textBuilder[textBuilder.Length - 1] == ',')
                {
                    textBuilder.Remove(textBuilder.Length - 1, 1);
                }
                textBuilder.Append("}");
            }
        }
    }
}
